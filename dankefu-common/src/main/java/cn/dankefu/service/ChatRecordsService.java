package cn.dankefu.service;

import cn.dankefu.bean.Chat_records;

/**
 * author: 蛋蛋的忧伤
 * date: 2018/5/19 0019 14:05
 */
public interface ChatRecordsService extends BaseService<Chat_records> {
}
